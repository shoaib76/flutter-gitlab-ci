# flutter_gitlab_ci

An example working Gitlab CI configuration for flutter projects. You can see the pipeline in action here [Flutter Gitlab CI in action](https://gitlab.com/Agraphie/flutter-gitlab-ci/pipelines/73572406). The publishing and release build steps are not working for this app however, as this is just an example app.
This project was inspired by [How to publish Android apps to the Google Play Store with GitLab and fastlane](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/)

![](/art/release_pipeline.jpg)

## Getting Started

This project provides a working example for Gitlab CI to build flutter applications. It makes use Docker images so that the flutter and Android SDKs don't have be downloaded everytime. Furthermore, you can publish and directly promote app bundles or APKs to the PlayStore. This follows the philosophy that one artifact has to pass through all stages before it can be promoted to production. 

### Setting things up

You will have to configure a few environment variables in Gitlab for this pipeline to work correctly.
- `google_play_service_account_api_key_json` --> The service account key for uploading APKs
- `signing_jks_file_hex` --> the signing keystore
- `signing_key_alias` --> the signing key alias
- `signing_key_password` --> the signing key password
- `signing_keystore_file` --> the signing keystore file location, e.g. `../upload_keystore.jks`
- `signing_keystore_password`--> the signing keystore password

For more informations, see this example [How to publish Android apps to the Google Play Store with GitLab and fastlane](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/)

Now you are ready to go!